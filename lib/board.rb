class Board

  attr_accessor :grid

  def initialize(grid = nil)
    @grid = grid || Board.default_grid
  end

  def self.default_grid
    @grid = Array.new(10) { Array.new(10) }
  end

  def count
    @grid.flatten.count { |ele| ele == :s }
  end

  def [](position)
    @grid[position[0]][position[1]]
  end

  def empty?(postion = nil)
    if postion != nil
      @grid[postion[0]][postion[1]] == nil ? true : false
    else
      if @grid.flatten.count { |ele| ele == nil } == @grid.flatten.count
        true
      else
        false
      end
    end
  end

  def full?
    if @grid.flatten.count { |ele| ele != nil } == @grid.flatten.count
      true
    else
      false
    end
  end

  def place_random_ship
    if full?
      raise error
    end
    grid_length = @grid.length
    random_1 = rand(grid_length)
    random_2 = rand(grid_length)
    if @grid[random_1][random_2] != nil
      place_random_ship
    else
      @grid[random_1][random_2] = :s
    end
  end

  def won?
    # if @grid.flatten.count(nil) == @grid.flatten.count
    #   return false
    # end
    @grid.flatten.any? { |ele| ele == :s} == true ? false : true
  end


end
