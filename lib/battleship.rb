class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def count
    @board.count
  end

  def play
    until game_over?
      $stdout.flush
      display
      play_turn
    end
    "Game over"
  end

  def get_play
    puts "Input where to hit"
    position = gets.chomp.split("")
    play = [position[0].to_i, position[3].to_i]
    if valid_play?(play) == false
      puts "****INVALID MOVE****"
      get_play
    else
      play
    end
  end

  def valid_play?(play)
    if play[0].class == Fixnum && play[1].class == Fixnum
      true
    else
      false
    end
  end

  def play_turn
    attack(@player.get_play)
  end

  def attack(position)
    mark = @board.grid[position[0]][position[1]]
    if hit?(mark)
      @board.grid[position[0]][position[1]] = :o
    else
      @board.grid[position[0]][position[1]] = :x
    end
  end

  def hit?(position)
    position == :s ? true : false
  end


  def display
    idx = 0
    column = (0...@board.grid.length).to_a.join(" ")
    print "  #{column}"
    puts ""
    @board.grid.length.times do
      inner = 0
      print "#{idx} "
      @board.grid.length.times do
        char = "* "
        if @board.grid[idx][inner] == :x
          char = "x "
        elsif @board.grid[idx][inner] == :o
          char = "o "
        end
        print char
        inner += 1
      end
      print " #{idx}"
      puts ""
      idx += 1
    end
    print "  #{column}"
    puts ""
  end



  def game_over?
    @board.won?
  end


end
